---
title: '08月30日'
date: 2020-08-30 00:00:00
categories:
  - 2020年08月
tags:
  - twitter
---

映画 #おかあさんの被爆ピアノ
幼少期の菜々子役の #竹井梨乃 ちゃんとのオフショット
:musical_keyboard:
:sparkles:

カメラを向けたら誰よりもアイドルなポーズをしてくれました
:joy:
:two_hearts:

本当に可愛い愛おしい〜
:heart_eyes:

![](/images/202008/twi20200830-1.jpg '20200830')
